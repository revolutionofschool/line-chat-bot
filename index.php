<?php

/********************************************************************/
/*          INITIATE BY SOBAT SISWA WITH LINE SDK                   */
/********************************************************************/

// Composer
	require __DIR__ . '/vendor/autoload.php';

// Modul
	use \LINE\LINEBot\SignatureValidator as SignatureValidator;

// load Env
	$dotenv		=	new Dotenv\Dotenv(__DIR__);
	$dotenv->load();

// Load Config
	$configs =  [
		'settings' => ['displayErrorDetails' => true],
	];
	$app = new Slim\App($configs);

// Routes
	// Check Functionality
		$app->get('/', function ($request, $response) {
			// Init Chat Bot
				$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($_ENV['CHANNEL_ACCESS_TOKEN']);
				$bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $_ENV['CHANNEL_SECRET']]);
				$textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder("Heiyo, someone's accessing your endpoint");
				$response = $bot->pushMessage('Uaf7ecd6c2e46a640601feec5b1a6acae', $textMessageBuilder);
				return "Its on !";
		});
	
	// Line Chat Bot Client
		$app->post('/', function ($request, $response)
		{
			// Get Signature
				$body 	   = file_get_contents('php://input');
				$signature = $_SERVER['HTTP_X_LINE_SIGNATURE'];

			// log body and signature
				file_put_contents('php://stderr', 'Body: '.$body);

			// Is LINE_SIGNATURE exists in request header?
				if (empty($signature)){
					return $response->withStatus(400, 'Signature not set');
				}

			// Is this request comes from LINE?
				if($_ENV['PASS_SIGNATURE'] == false && ! SignatureValidator::validateSignature($body, $_ENV['CHANNEL_SECRET'], $signature)){
					return $response->withStatus(400, 'Invalid signature');
				}

			// Init Chat Bot
				$httpClient		= new \LINE\LINEBot\HTTPClient\CurlHTTPClient($_ENV['CHANNEL_ACCESS_TOKEN']);
				$bot			= new \LINE\LINEBot($httpClient, ['channelSecret' => $_ENV['CHANNEL_SECRET']]);
				$guzzleClient	= new \GuzzleHttp\Client();
				$data			= json_decode($body, true);
			
			// Check Event
				foreach ($data['events'] as $event)
				{
					// Get Message
						$basicUrl 	=	"https://app.sobatsiswa.id/v1/";
						$userMessage	= strtolower($event['message']['text']);
						$userSource		= $event["source"]["userId"];
						$userArray 		= explode(" ", $userMessage);
						$authToken 	=	['headers' => ['Authorization' => $userSource]];
					
					// Start Branch

						// Development
							if ($userArray[0] == "dev") {
								// Set Action
									if (in_array("--source", $userArray)) {
										// Get Source
											$message	=  $userSource;
									} else {
										// Command Not Found
											$message	=	"Command not found";
									}
							}

						// Login
							else if (in_array($userArray[0], ["login"])) {
								// Count User Message
									if (count($userArray) == 4) {
										// Guzzle Data
											$guzzleData['school_code']	=	$userArray[1];
											$guzzleData['username']		=	$userArray[2];
											$guzzleData['password']		=	$userArray[3];
											$guzzleData['role']			=	2;
											$guzzleData['line_account']	=	true;
											$guzzleData['line_source']	=	$userSource;
										// Sending Request
											try {
												$guzzleResponse				=	$guzzleClient->post(($basicUrl . "login"), ["json" => $guzzleData]);
												$accountInformation			=	json_decode($guzzleResponse->getBody()->getContents());
												$message					=	"Hai " . $accountInformation->nickname . ", akun Line kamu sudah terhubung dengan akun Sobat Siswa yah. 🎆";
											} catch (\GuzzleHttp\Exception\RequestException $e) {
												$message				=	"Yah, akun yang kamu masukkan tidak dikenali nih. Coba diperiksa kembali yaa 👀";
											}
									}
								// If Login Failed
									else {
										$message	=	"Maaf ya, proses login kamu gagal. Cip cip cip 💢️";
									}
							}
						
						// Logout
							else if (in_array($userArray[0], ["logout", "keluar"])) {
								// Sending Request
									try {
										// Set Response and Message
											$guzzleResponse				=	$guzzleClient->get(($basicUrl . "logout"), $authToken);
											$message					=	"Logout berhasil ! Akun Line kamu sudah tak terhubung lagi dengan akun Sobat Siswa. 🔅";
									} catch (\GuzzleHttp\Exception\RequestException $e) {
										$message				=	"Yah, akun yang kamu masukkan tidak dikenali nih. Coba diperiksa kembali yaa 👀";
									}
							}

						// School
							else if (in_array($userArray[0], ["sekolahku"])) {
								// Sending Request
									try {
										// Set Response and Message
											$guzzleResponse				=	$guzzleClient->get(($basicUrl . "school"), $authToken);
											$accountInformation			=	json_decode($guzzleResponse->getBody()->getContents());
											$message[0]					=	"🏫 Sekolahmu adalah " . $accountInformation->name . " yang berada di " . $accountInformation->address . ".";;
											$message[1]					=	"📞 Jika kamu memiliki keperluan, kamu bisa menghubungi sekolahmu lewat " . $accountInformation->phone . " atau melalui email di " . $accountInformation->email . ".";
										// Sending Message
											$textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($message[0], $message[1]);
									} catch (\GuzzleHttp\Exception\RequestException $e) {
										$message				=	"Yah, akun yang kamu masukkan tidak dikenali nih. Coba diperiksa kembali yaa 👀";
									}
							}
						
						// Help
							else if (in_array($userArray[0], ["help", "bantuan"])) {
								// Rule Definition
									$message	= 	[
														"1️⃣ Jika kamu ingin melakukan proses login, ketikkan : login kode_sekolah nomor_induk kata_sandi",
														"2️⃣ Jika kamu ingin keluar atau memutuskan akun line kamu dengan akun sobat siswa, ketikkan : logout"
													];
								// Set Response
									$textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($message[0], $message[1]);
							}

						// Other
							else {
								$message	= "Sedang dalam masa pengembangan ya. Tunggu saatnya oke, cip cip cip 🔅 ";
							}
					
					// Send Response
						//	Message Is Array Or String
							if (!is_array($message)) {
								$textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($message);
							}
						// Sending Feedback
							$result = $bot->replyMessage($event['replyToken'], $textMessageBuilder);
							return $result->getHTTPStatus() . ' ' . $result->getRawBody();
				}

		});

// Run Server
	$app->run();